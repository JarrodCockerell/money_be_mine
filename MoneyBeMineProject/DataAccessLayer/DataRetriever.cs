﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class DataRetriever
    {
        private StockExchangeEntities _Entities = new StockExchangeEntities();

        //  Returns the stocks from the database in a list
        public List<Stock> GetDataFromDatabase()
        {
            List<Stock> returnList = new List<Stock>();
            foreach (var item in _Entities.Stocks.AsEnumerable())
            {
                returnList.Add(item);
            }
            return returnList;
        }
    }
}
